# Link orientação

# Orientação

Este projeto foi criando na base de arquétipo que venho desenvolvendo com base no meu conhecimento e no aprendizado do dia a dia.

O que desenvolvido:

 - Controllers
 - Services
 - Dtos
 - Models
 - Repositóries
 - Feign external
 - JUnits

As implementações que está no pacote infra faz parte do arquetipo maven fruto de estudos e cursos

# Ideia do Projeto

Foi criado uma api que gerencia o cadastro basico de clientes com o objetivo de expressar meu conhecimento em SpringBoot, Rest, JPA e outras frameworks que auxiliam no desenvolmento e escrita de código.

# Spring

- Versão: 2.3.4.RELEASE

# Documentação swagger

URL: http://localhost:8082/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/

# Regras:

Algumas regras foram aplicadas na api de cliente como:

- Só pode ser cadastrado um cliente por e-mail
- É obirgatório informar um telefone

# Chamar uma API qualquer de terceiros

Acesso: https://viacep.com.br/

Consulta por CEP: https://viacep.com.br/ws/01001000/json/

# Postman

Teste automátizados da API Banco Original.postman_collection.json.

# Teste das APIs

Executando testes da API com Postman com newman:

Tutorial: https://renatogroffe.medium.com/automatizando-testes-de-apis-rest-com-postman-newman-a90f0d90df09

Windows: newman run "Banco Original.postman_collection.json" -e LOCALHOST.postman_environment.json
Linux:  newman run Banco\ Original.postman_collection.json -e LOCALHOST.postman_environment.json

# Considerações

Confesso que desenvolvi muito código com base no que foi proposto, mas entendo o que foi desenvolvido é uma API simples.