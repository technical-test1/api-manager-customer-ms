FROM openjdk:11-jre
VOLUME /tmp
EXPOSE 8082/tcp
COPY target/api-manager-customer-ms.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
