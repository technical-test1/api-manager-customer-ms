package br.com.leandro.original.api.service.mapper;


import org.mapstruct.Mapper;

import br.com.leandro.original.api.repository.model.PhoneModel;
import br.com.leandro.original.api.service.dto.PhoneSaveDTO;

@Mapper(componentModel = "spring", uses = {})
public interface PhoneSaveMapper extends ConveterMapper<PhoneSaveDTO, PhoneModel> {

}
