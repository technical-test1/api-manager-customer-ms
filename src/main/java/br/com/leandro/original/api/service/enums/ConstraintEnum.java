package br.com.leandro.original.api.service.enums;

public enum ConstraintEnum {

	UNDEFINED("UNDEFINED"),
	UK_CLIENTE_EMAIL( Constraint.UK_CLIENTE_EMAIL ),
	;

	private String name;

	private ConstraintEnum(String name ) {
		this.name = name;
	}

	public static ConstraintEnum getConstraint( String messageErro ) {
		for ( ConstraintEnum v : ConstraintEnum.values() ) {
			if( messageErro.contains( v.getName() )) {
				return v;
			}
		}
		return UNDEFINED;
	}

	public String getName() {
		return name;
	}
}
