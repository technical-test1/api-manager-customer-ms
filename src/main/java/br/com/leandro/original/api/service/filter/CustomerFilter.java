package br.com.leandro.original.api.service.filter;

import java.io.Serializable;

import lombok.Data;

@Data
public class CustomerFilter implements Serializable {

	private String name;

	private String email;

	private String cpf;

}
