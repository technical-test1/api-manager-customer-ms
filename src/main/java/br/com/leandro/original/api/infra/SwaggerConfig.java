package br.com.leandro.original.api.infra;


import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@Configuration
@OpenAPIDefinition(info = @Info(
    title = "Leandro Marques da Cunha - Teste tecnico - Banco Original",
    version = "${app.version}",
    description = "CRUD Cliente e Consulta de CEP"
))
public class SwaggerConfig {


}
