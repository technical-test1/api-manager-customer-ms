package br.com.leandro.original.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.leandro.original.api.repository.model.PhoneModel;

@Repository
public interface PhoneRepository extends JpaRepository<PhoneModel, Long>{

	void deleteAllByCustomerId( Long id );
}
