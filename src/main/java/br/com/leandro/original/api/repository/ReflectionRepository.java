package br.com.leandro.original.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.leandro.original.api.repository.model.ReflectionModel;

public interface ReflectionRepository extends JpaRepository<ReflectionModel, Long>{

}
