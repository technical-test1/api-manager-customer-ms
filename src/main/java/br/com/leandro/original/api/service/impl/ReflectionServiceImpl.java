package br.com.leandro.original.api.service.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.leandro.original.api.infra.dto.ReflectionDTO;
import br.com.leandro.original.api.repository.ReflectionRepository;
import br.com.leandro.original.api.repository.model.ReflectionModel;
import br.com.leandro.original.api.service.ReflectionService;
import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class ReflectionServiceImpl implements ReflectionService {

	private final ReflectionRepository reflectionRepository;

	@Override
	@Transactional
	public Map<String, Object> reflection(ReflectionDTO reflectionDTO) {

		List<Field> fields = Arrays.asList( reflectionDTO.getClass().getDeclaredFields() );

		List<ReflectionModel> models = new ArrayList<>();
		fields.forEach( f ->  {
			try {

				Boolean access =  f.canAccess(reflectionDTO);

				f.setAccessible( Boolean.TRUE );

				Object value = f.get( reflectionDTO );

				ReflectionModel model = ReflectionModel
											.builder()
											.key( f.getName() )
											.typeValue( value.getClass().getTypeName() )
											.value( String.valueOf( value ) )
											.build();
				f.setAccessible( access );

				models.add(model);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}

		} );

		reflectionRepository.saveAll( models );

		List<ReflectionModel> findAll = reflectionRepository.findAll();


		Map<String, Object> result = new HashedMap<>();


		for (ReflectionModel reflectionModel : findAll) {

			try {

				Object value = reflectionModel.getValue();

				if( BooleanUtils.isFalse( reflectionModel.getTypeValue().contains("LocalDate")  ) ) {
					Class<?> extensionsClass = Class.forName(reflectionModel.getTypeValue());
					extensionsClass.getDeclaredConstructors();
					Constructor<?> constructor = extensionsClass.getDeclaredConstructor(String.class);
					value =  constructor.newInstance( reflectionModel.getValue() );
				}

				result.put( reflectionModel.getKey(), value);

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

}
