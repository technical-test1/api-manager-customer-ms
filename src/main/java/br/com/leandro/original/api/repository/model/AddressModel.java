package br.com.leandro.original.api.repository.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table
@Entity( name = "TB_ENDERECO")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@PrimaryKeyJoinColumn(name = "CG_CLIENTE" )
public class AddressModel implements Serializable {

	private static final long serialVersionUID = 8924708543465135362L;

	@Id
	@SequenceGenerator(name = "TB_ENDERECO_GENERATOR", sequenceName = "SEQ_ENDERECO", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_ENDERECO_GENERATOR")
	@Column(name = "CG_ENDERECO")
	@EqualsAndHashCode.Include
	private Long id;

	@OneToOne( cascade = CascadeType.ALL)
	@JoinColumn(name = "CG_CLIENTE", foreignKey = @ForeignKey( value =ConstraintMode.CONSTRAINT , name = "FK_CLIENTE_ENDERECO" ) )
	@ToString.Exclude
	private CustomerModel customer;

	@Column(name = "TE_LOGRADOURO")
	private String address;

	@Column(name = "TE_NUMERO")
	private String number;

	@Column(name = "TE_CEP")
	private String zip;

	@Column(name = "TE_COMPLEMENTO")
	private String complement;

	@Column(name = "TE_BAIRRO")
	private String district;

	@Column(name = "TE_CIDADE")
	private String city;

	@Column(name = "TE_UF")
	private String state;

}
