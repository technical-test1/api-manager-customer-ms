package br.com.leandro.original.api.service.dto;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Tag(name = "ClienteDTO" )
@Schema(name = "ClienteDTO", description = "Informações do cliente")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO  {

	public static final String CACHE_NAME = "customersCache";

	@JsonProperty("id")
	@Schema(name = "id", description = "Código único do cliente")
	private Long id;

	@JsonProperty("name")
	@Schema(name = "name", description = "Nome do cliente")
	private String name;

	@JsonProperty("email")
	@Schema(name = "email", description = "E-mail do cliente")
	private String email;

	@JsonProperty("cpf")
	@Schema(name = "cpf", description = "CPF do cliente")
	private String cpf;

	@JsonProperty("address")
	@Schema(name = "address", description = "Endereco do cliente")
	private AddressDTO address;

	@JsonProperty("phones")
	@Schema(name = "phones", description = "Telefones do cliente")
	private List<PhoneDTO> phones;

}
