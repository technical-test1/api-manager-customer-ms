package br.com.leandro.original.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.leandro.original.api.infra.dto.ResponseDTO;
import br.com.leandro.original.api.service.ViaCepService;
import br.com.leandro.original.api.service.dto.viacep.AddressViaCep;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/viacep")
@AllArgsConstructor
@Tag(name = "ZipController", description = "Manager Seach zip")
@Slf4j
public class ZipController {

	private ViaCepService viaCepService;

	@GetMapping("/v1/zip/{zip-code}")
	public ResponseEntity<ResponseDTO<AddressViaCep>> findById( @PathVariable("zip-code") String zip ){
		log.info("Buscando a lista de clientes por id {}" , zip );
		AddressViaCep addressViaCep = viaCepService.findByZip(zip);
		log.info("Busca efetuada com sucesso ");
		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( addressViaCep ));
	}

}
