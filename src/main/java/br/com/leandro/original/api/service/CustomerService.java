package br.com.leandro.original.api.service;

import br.com.leandro.original.api.infra.dto.PageDTO;
import br.com.leandro.original.api.service.dto.CustomerDTO;
import br.com.leandro.original.api.service.dto.CustomerSaveDTO;
import br.com.leandro.original.api.service.filter.CustomerFilter;

public interface CustomerService {

	CustomerDTO save(CustomerSaveDTO customerSaveDTO);

	CustomerDTO update(Long id, CustomerSaveDTO customerSaveDTO);

	void remove(Long id);

	PageDTO<CustomerDTO> findByCustomerFilter(CustomerFilter customerFilter, Integer page, Integer size);

	CustomerDTO findById( Long id );

}
