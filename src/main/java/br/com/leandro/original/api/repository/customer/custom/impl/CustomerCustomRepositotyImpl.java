package br.com.leandro.original.api.repository.customer.custom.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import br.com.leandro.original.api.repository.customer.custom.CustomerCustomRepositoty;
import br.com.leandro.original.api.repository.model.CustomerModel;
import br.com.leandro.original.api.service.filter.CustomerFilter;

public class CustomerCustomRepositotyImpl implements CustomerCustomRepositoty {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<CustomerModel> findByCustomerFilter(CustomerFilter filter, Pageable sortedByName) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CustomerModel> criteria = builder.createQuery(CustomerModel.class);
		Root<CustomerModel> root = criteria.from(CustomerModel.class);

		Predicate[] predicates = createRestrictions(filter, builder, root);
		criteria.where(predicates);
		criteria.orderBy( (builder.desc(root.get("name"))));

		TypedQuery<CustomerModel> query = manager.createQuery(criteria);

		addRestrictionsOfPagnation(query, sortedByName );

		return new PageImpl<>(query.getResultList(), sortedByName, total(filter));
	}


	private Long total( CustomerFilter filter ) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<CustomerModel> root = criteria.from(CustomerModel.class);
		Predicate[] predicates = createRestrictions(filter, builder, root);
		criteria.where(predicates);
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	static void addRestrictionsOfPagnation(TypedQuery<CustomerModel> query, Pageable pageable) {
		int actualPage = pageable.getPageNumber();
		int totalForPage = pageable.getPageSize();
		int firstPage = actualPage * totalForPage;

		query.setFirstResult(firstPage);
		query.setMaxResults(totalForPage);
	}

	private Predicate[] createRestrictions(CustomerFilter filter, CriteriaBuilder builder, Root<CustomerModel> root) {
		List<Predicate> predicates = new ArrayList<>();

		if (StringUtils.isNotBlank(filter.getName())) {
			predicates.add(builder.like( builder.upper(root.get( "name") ),
					StringUtils.upperCase( "%" +filter.getName().trim() + "%") ));
		}

		if (StringUtils.isNotBlank(filter.getEmail())) {
			predicates.add(builder.like( builder.upper( root.get("email") ),
					StringUtils.upperCase( "%" + filter.getEmail().trim() + "%") ));
		}

		if (StringUtils.isNotBlank(filter.getCpf())) {
			predicates.add(builder.equal( builder.upper( root.get("cpf") ),
					StringUtils.upperCase( filter.getCpf() )));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}
}