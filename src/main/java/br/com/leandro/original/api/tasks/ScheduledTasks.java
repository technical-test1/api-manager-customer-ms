package br.com.leandro.original.api.tasks;

import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

import static net.javacrumbs.shedlock.core.LockAssert.assertLocked;

@Component
public class ScheduledTasks {

	@Scheduled(fixedRate = 100)
    @SchedulerLock(name = "reportCurrentTime", lockAtMostFor = "${lock.at.most.for}")
    public void reportCurrentTime() {
        assertLocked();
        System.out.println(new Date());
    }
}
