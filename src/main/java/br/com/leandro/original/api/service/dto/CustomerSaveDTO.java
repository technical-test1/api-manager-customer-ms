package br.com.leandro.original.api.service.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Tag(name="CustomerSaveDTO")
@Schema(name = "CustomerSaveDTO", description = "Informações que serão gravadas do cliente")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerSaveDTO {

	@JsonProperty("name")
	@Schema(name = "name", description = "Nome do cliente. Informação Obrigatória", required = true)
	@NotNull(message = "cliente.nome.not.nulo")
	@NotEmpty(message = "cliente.nome.invalido")
	private String name;

	@JsonProperty("address")
	@Schema(name = "address", description = "Endereco do cliente")
	private AddressSaveDTO address;

	@JsonProperty("email")
	@Schema(name = "email", description = "E-mail do cliente. Obrigatório e não poder repetir", required = true)
	@NotBlank(message = "cliente.email.not.nulo")
	private String email;


	@JsonProperty("cpf")
	@Schema(name = "cpf", description = "cpf do cliente.", required = false)
	private String cpf;

	@JsonProperty("phones")
	@Schema(name = "phones", description = "Telefones do cliente. Informar no minimo um telefone", required = true)
	@NotNull(message = "cliente.telefones.not.nulo")
	@Valid
	private List<PhoneSaveDTO> phones;

}
