package br.com.leandro.original.api.service.impl;

import org.springframework.stereotype.Service;

import br.com.leandro.original.api.infra.handler.exception.ApiException;
import br.com.leandro.original.api.infra.handler.util.ExceptionCodeEnum;
import br.com.leandro.original.api.service.ViaCepService;
import br.com.leandro.original.api.service.dto.viacep.AddressViaCep;
import feign.FeignException;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ViaCepServiceImpl implements ViaCepService {

	private SearchViaCepService searchViaCepService;

	@Override
	public AddressViaCep findByZip(String zip) {
		try {
			return searchViaCepService.findByZip(zip);
		}catch (FeignException e) {
			throw new ApiException(ExceptionCodeEnum.ERRO_AO_CONSULTAR_API_CEP, e);
		}
	}

}
