package br.com.leandro.original.api.service.mapper;


import org.mapstruct.Mapper;

import br.com.leandro.original.api.repository.model.CustomerModel;
import br.com.leandro.original.api.service.dto.CustomerDTO;

@Mapper(componentModel = "spring", uses = {AddressMapper.class, PhoneMapper.class})
public interface CustomerMapper extends ConveterMapper<CustomerDTO, CustomerModel> {

}
