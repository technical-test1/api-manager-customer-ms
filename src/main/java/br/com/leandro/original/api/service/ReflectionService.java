package br.com.leandro.original.api.service;

import java.util.Map;

import br.com.leandro.original.api.infra.dto.ReflectionDTO;


public interface ReflectionService {

	Map<String,Object> reflection( ReflectionDTO reflectionDTO );
}
