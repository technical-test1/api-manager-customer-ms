package br.com.leandro.original.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.leandro.original.api.repository.model.AddressModel;

@Repository
public interface AddressRepository extends JpaRepository<AddressModel, Long>{

	void deleteAllByCustomerId( Long id );
}
