package br.com.leandro.original.api.repository.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table
@Entity( name = "TB_TELEFONE")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PhoneModel implements Serializable {

	private static final long serialVersionUID = -7929425622274565356L;

	@Id
	@SequenceGenerator(name = "TB_TELEFONE_GENERATOR", sequenceName = "SEQ_TELEFONE", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_TELEFONE_GENERATOR")
	@Column(name = "CG_TELEFONE")
	@EqualsAndHashCode.Include
	private Long id;

	@Column(name = "NU_DDD")
	private Long ddd;

	@Column(name = "NU_NUMERO")
	private Long number;

	@ManyToOne
	@JoinColumn(name = "CG_CLIENTE", foreignKey = @ForeignKey( value =ConstraintMode.CONSTRAINT , name = "FK_CLIENTE_TELEFONE"))
	@ToString.Exclude
	private CustomerModel customer;

}
