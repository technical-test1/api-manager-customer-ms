package br.com.leandro.original.api.infra.logback;

public interface SensibleDataFormatter {

	boolean handle(String message);

	String format(String message);

}