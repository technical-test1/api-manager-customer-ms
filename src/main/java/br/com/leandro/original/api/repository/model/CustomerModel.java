package br.com.leandro.original.api.repository.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.leandro.original.api.service.enums.Constraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Table(uniqueConstraints = @UniqueConstraint( columnNames = "TE_EMAIL", name = Constraint.UK_CLIENTE_EMAIL))
@Entity( name = "TB_CLIENTE")
@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
public class CustomerModel implements Serializable {

	private static final long serialVersionUID = -7929425622274565356L;

	@Id
	@SequenceGenerator(name = "TB_CLIENTE_GENERATOR", sequenceName = "SEQ_CLIENTE", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_CLIENTE_GENERATOR")
	@Column(name = "CG_CLIENTE")
	@EqualsAndHashCode.Include
	private Long id;

	@Column(name = "TE_NOME")
	private String name;

	@NotNull(message = "cliente.email.not.nulo")
	@NotEmpty(message = "cliente.email.not.nulo")
	@Column(name = "TE_EMAIL" , nullable = false ,insertable = true)
	private String email;

	@Column(name = "TE_CPF" )
	private String cpf;

	@OneToOne(mappedBy = "customer", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private AddressModel address;

	@OneToMany(mappedBy = "customer", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<PhoneModel> phones;

	public void setPhones(List<PhoneModel> phones) {
		phones.forEach((PhoneModel phone ) -> phone.setCustomer(this));
		this.phones = phones;
	}

	public void setAddress(AddressModel address ) {
		address.setCustomer(this);
		this.address = address;
	}
}
