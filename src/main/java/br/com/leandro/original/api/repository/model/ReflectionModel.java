package br.com.leandro.original.api.repository.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table
@Entity( name = "TB_REFLECTION")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ReflectionModel implements Serializable {

	private static final long serialVersionUID = 8924708543465135362L;

	@Id
	@SequenceGenerator(name = "TB_REFLECTION", sequenceName = "SEQ_REFLECTION", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TB_REFLECTION")
	@Column(name = "CG_REFLECTION")
	@EqualsAndHashCode.Include
	private Long id;

	@Column(name = "TE_KEY")
	private String key;

	@Column(name = "TE_VALUE")
	private String value;

	@Column(name = "TE_TYPE_VALUE")
	private String typeValue;

}
