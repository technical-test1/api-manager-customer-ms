package br.com.leandro.original.api.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import br.com.leandro.original.api.repository.model.CustomerModel;
import br.com.leandro.original.api.service.dto.CustomerSaveDTO;

@Mapper(componentModel = "spring", uses = { AddressSaveMapper.class, PhoneSaveMapper.class})
public interface CustomerSaveMapper extends ConveterMapper<CustomerSaveDTO, CustomerModel> {



	@Mapping(target = "id", ignore = true)
	@Mapping(target = "phones" , expression = "java( phoneSaveMapper.toEntity( dto.getPhones() ) )" )
	CustomerModel updateEntity(@MappingTarget  CustomerModel entity, CustomerSaveDTO dto);
}
