package br.com.leandro.original.api.controller;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.leandro.original.api.infra.dto.ReflectionDTO;
import br.com.leandro.original.api.infra.dto.ResponseDTO;
import br.com.leandro.original.api.service.ReflectionService;
import br.com.leandro.original.api.service.ViaCepService;
import br.com.leandro.original.api.service.dto.viacep.AddressViaCep;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/example")
@RequiredArgsConstructor
@Tag(name = "ReflectionController", description = "Example reflection")
@Slf4j
public class ReflectionController {

	private final ReflectionService reflectionService;

	@PostMapping("/v1/reflection")
	public ResponseEntity<ResponseDTO<Map<String, Object>>> reflection( @RequestBody ReflectionDTO reflectionDTO ){
		log.info("Buscando a lista de clientes por id {}" , reflectionDTO );
		Map<String, Object> response = reflectionService.reflection(reflectionDTO);
		log.info("Busca efetuada com sucesso ");
		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( response ));
	}

}
