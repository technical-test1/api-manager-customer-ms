package br.com.leandro.original.api.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Tag(name = "PhoneDTO")
@Schema(name = "PhoneDTO", description = "Telefones do cliente")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PhoneDTO  {

	@Schema(name = "id", description =  "Código único do telefone do cliente")
	@EqualsAndHashCode.Include
	private Long id;

	@JsonProperty("ddd")
	@Schema(name = "ddd", description =  "DDD")
	private Long ddd;

	@JsonProperty("number")
	@Schema(name = "number", description =  "Numero do Telefone")
	private Long number;
}
