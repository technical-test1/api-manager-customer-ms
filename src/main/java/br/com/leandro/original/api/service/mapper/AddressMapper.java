package br.com.leandro.original.api.service.mapper;


import org.mapstruct.Mapper;

import br.com.leandro.original.api.repository.model.AddressModel;
import br.com.leandro.original.api.service.dto.AddressDTO;

@Mapper(componentModel = "spring", uses = {})
public interface AddressMapper extends ConveterMapper<AddressDTO, AddressModel> {

}
