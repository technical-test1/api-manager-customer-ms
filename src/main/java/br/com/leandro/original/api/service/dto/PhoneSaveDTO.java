package br.com.leandro.original.api.service.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Tag( name = "PhoneSaveDTO")
@Schema(name = "PhoneSaveDTO", description = "Telefone do cliente")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhoneSaveDTO  {


	@JsonProperty("ddd")
	@Schema(name =  "ddd", description = "DDD. Obrigatório.", required = true )
	@NotNull(message = "telefone.ddd.not.nulo")
	@Min(value = 1, message = "telefone.ddd.not.zero")
	private Long ddd;

	@JsonProperty("number")
	@Schema(name =  "number", description =  "Numero do Telefone. Obrigatório.", required = true)
	@NotNull(message = "telefone.numero.not.nulo")
	@Min(value = 1, message = "telefone.numero.not.zero")
	private Long number;
}
