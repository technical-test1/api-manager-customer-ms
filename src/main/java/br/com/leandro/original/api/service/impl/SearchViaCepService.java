package br.com.leandro.original.api.service.impl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.leandro.original.api.service.dto.viacep.AddressViaCep;

@FeignClient(name = "via-cep", url = "${app.url.viacep}")
public interface SearchViaCepService {

	@GetMapping( path = "{zip-code}/ws/json", consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE )
	AddressViaCep findByZip( @PathVariable("zip-code") String zip );
}
