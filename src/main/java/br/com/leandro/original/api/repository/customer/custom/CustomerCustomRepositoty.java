package br.com.leandro.original.api.repository.customer.custom;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.leandro.original.api.repository.model.CustomerModel;
import br.com.leandro.original.api.service.filter.CustomerFilter;

public interface CustomerCustomRepositoty {

	Page<CustomerModel> findByCustomerFilter( CustomerFilter filter, Pageable sortedByName );
}
