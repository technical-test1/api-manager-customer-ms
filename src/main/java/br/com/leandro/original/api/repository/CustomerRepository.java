package br.com.leandro.original.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.leandro.original.api.repository.customer.custom.CustomerCustomRepositoty;
import br.com.leandro.original.api.repository.model.CustomerModel;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerModel, Long>, CustomerCustomRepositoty {

}
