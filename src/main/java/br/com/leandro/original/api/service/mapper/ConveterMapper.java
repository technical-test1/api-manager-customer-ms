package br.com.leandro.original.api.service.mapper;


import java.util.List;

import org.mapstruct.Mapping;

public interface ConveterMapper<D, E> {

	@Mapping(target = "id", ignore = true)
	E toEntity(D dto);
	D toDto(E entity);

	List<E> toEntity(List<D> dtoList);
	List<D> toDto(List<E> entityList);

}
