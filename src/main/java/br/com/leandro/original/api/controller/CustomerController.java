package br.com.leandro.original.api.controller;

import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.leandro.original.api.infra.dto.PageDTO;
import br.com.leandro.original.api.infra.dto.ResponseDTO;
import br.com.leandro.original.api.infra.dto.ResponseErrorDTO;
import br.com.leandro.original.api.service.CustomerService;
import br.com.leandro.original.api.service.dto.CustomerDTO;
import br.com.leandro.original.api.service.dto.CustomerSaveDTO;
import br.com.leandro.original.api.service.filter.CustomerFilter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/customer")
@AllArgsConstructor
@Tag(name = "CustomerController", description = "Manager CRUD Customer")
@Slf4j
public class CustomerController {

	private CustomerService customerService;

	@Operation(summary = "Create a new customer", description = "Customer",
			responses = {
					@ApiResponse(
							responseCode = "201",
							description = "Customer created" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
											mediaType = MediaType.APPLICATION_JSON_VALUE ,
											schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
											mediaType = MediaType.APPLICATION_JSON_VALUE,
											schema = @Schema(implementation = ResponseErrorDTO.class)))
	})

	@PostMapping("/v1/customers" )
	public ResponseEntity<ResponseDTO<CustomerDTO>> save( @Valid @RequestBody CustomerSaveDTO clienteSaveDTO ){
		log.info("Iniciando a gravação de um novo clienteSave: {}" , clienteSaveDTO );
		CustomerDTO clienteDTO = customerService.save(clienteSaveDTO);
		return ResponseEntity
				.status( HttpStatus.CREATED )
				.body( new ResponseDTO<>( clienteDTO ));
	}


	@Operation(summary = "Update a customer", description = "Customer",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Customer updated" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
											mediaType = MediaType.APPLICATION_JSON_VALUE ,
											schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
											mediaType = MediaType.APPLICATION_JSON_VALUE,
											schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	@PutMapping("/v1/customers/{id}")
	public ResponseEntity<ResponseDTO<CustomerDTO>> update( @PathVariable("id") Long id,
			@Valid @RequestBody CustomerSaveDTO customerSaveDTO ){
		log.info("Iniciando a gravação das alterações dos dados do clienteSave: {}", customerSaveDTO );
		CustomerDTO customerDTO =	customerService.update( id , customerSaveDTO );

		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( customerDTO ));
	}

	@Operation(summary = "Delete a customer", description = "Customer",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Customer deleted" ),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
											mediaType = MediaType.APPLICATION_JSON_VALUE ,
											schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
											mediaType = MediaType.APPLICATION_JSON_VALUE,
											schema = @Schema(implementation = ResponseErrorDTO.class)))
	})

	@DeleteMapping("/v1/customers/{id}")
	public ResponseEntity<ResponseDTO<String>> remove( @PathVariable Long id ){
		log.info("Apagando o cliente por id" );
		customerService.remove(id);
		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( "Removido com sucesso" ));
	}

	@Operation(summary = "Search customer", description = "Customer",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Customer list" ),
					@ApiResponse(
							responseCode = "204",
							description = "List customer empty returned",
							content = @Content( mediaType = MediaType.APPLICATION_JSON_VALUE )),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
											mediaType = MediaType.APPLICATION_JSON_VALUE ,
											schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
											mediaType = MediaType.APPLICATION_JSON_VALUE,
											schema = @Schema(implementation = ResponseErrorDTO.class)))
	})
	@GetMapping("/v1/customers")
	public ResponseEntity<ResponseDTO<PageDTO<CustomerDTO>>> findByCustormerFilter( CustomerFilter customerFilter , Integer page, Integer size ){
		log.info("Buscando a lista de clientes com os parametros {}" , customerFilter );
		PageDTO<CustomerDTO> customerDTO = customerService.findByCustomerFilter( customerFilter ,  page , size );
		return ResponseEntity
				.status( CollectionUtils.isEmpty( customerDTO.getContent() )
						? HttpStatus.NO_CONTENT :  HttpStatus.OK )
				.body( new ResponseDTO<>( customerDTO ));
	}

	@Operation(summary = "Search customer by ID", description = "Customer",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Customer list" ),
					@ApiResponse(
							responseCode = "204",
							description = "List customer empty returned",
							content = @Content( mediaType = MediaType.APPLICATION_JSON_VALUE )),
					@ApiResponse(
							responseCode = "422",
							description = "Invalid body field",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = ResponseErrorDTO.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server Error",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = ResponseErrorDTO.class)))
	})

	@GetMapping("/v1/customers/{id}")
	public ResponseEntity<ResponseDTO<CustomerDTO>> findById( @PathVariable Long id ){
		log.info("Buscando a lista de clientes por id {}" , id );
		CustomerDTO customerDTO = customerService.findById( id );
		return ResponseEntity
				.status( HttpStatus.OK )
				.body( new ResponseDTO<>( customerDTO ));
	}

}
