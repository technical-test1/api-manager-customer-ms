package br.com.leandro.original.api.service.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Tag(name="AddressDTO")
@Schema(name = "AddressDTO", description = "Endereço do cliente")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class AddressDTO {

	@JsonProperty("id")
	@Schema(name = "id", description = "Código único do endereço cliente")
	@EqualsAndHashCode.Include
	private Long id;

	@JsonProperty("address")
	@Schema(name = "address", description = "Logradouro")
	private String address;

	@JsonProperty("number")
	@Schema(name = "number", description = "Númedo da residência")
	private String number;

	@JsonProperty("zip")
	@Schema(name = "zip", description = "CEP da residência")
	private String zip;

	@JsonProperty("complement")
	@Schema(name = "complement", description = "Complemento")
	private String complement;

	@JsonProperty("district")
	@Schema(name = "district", description = "Bairro")
	private String district;

	@JsonProperty("city")
	@Schema(name = "city", description = "Cidade")
	private String city;

	@JsonProperty("state")
	@Schema(name = "state", description = "UF")
	private String state;

}
