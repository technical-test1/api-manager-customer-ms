package br.com.leandro.original.api.infra.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties( "app" )
public class ApiProperties {

	private String version;

	public void setVersion(String version) {
		this.version = version;
	}

	public String getVersion() {
		return this.version;
	}

}
