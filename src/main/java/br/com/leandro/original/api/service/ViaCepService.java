package br.com.leandro.original.api.service;

import br.com.leandro.original.api.service.dto.viacep.AddressViaCep;


public interface ViaCepService {

	AddressViaCep findByZip( String zip );
}
