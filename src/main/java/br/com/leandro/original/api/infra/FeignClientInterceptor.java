package br.com.leandro.original.api.infra;

import java.util.Collection;
import java.util.Map;

import org.springframework.stereotype.Component;

import br.com.leandro.original.api.infra.dto.ApiHearderRequest;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class FeignClientInterceptor implements RequestInterceptor {

	private final ApiHearderRequest hearderRequest;

	@Override
	public void apply(RequestTemplate template) {
		Map<String, Collection<String>> param = hearderRequest.config( template.feignTarget().name() );
		template.headers(param);
		log.info("Aqui configuramos o hearder das requisiçoes");
		log.info("Passou pelo Interceptor: FeignClientInterceptor");
	}
}
