package br.com.leandro.original.api.service.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.leandro.original.api.infra.dto.PageDTO;
import br.com.leandro.original.api.infra.handler.exception.ApiException;
import br.com.leandro.original.api.infra.handler.util.ExceptionCodeEnum;
import br.com.leandro.original.api.repository.CustomerRepository;
import br.com.leandro.original.api.repository.PhoneRepository;
import br.com.leandro.original.api.repository.model.CustomerModel;
import br.com.leandro.original.api.service.CustomerService;
import br.com.leandro.original.api.service.dto.CustomerDTO;
import br.com.leandro.original.api.service.dto.CustomerSaveDTO;
import br.com.leandro.original.api.service.filter.CustomerFilter;
import br.com.leandro.original.api.service.mapper.CustomerMapper;
import br.com.leandro.original.api.service.mapper.CustomerSaveMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class CustomerServiceImpl implements CustomerService {

	private CustomerRepository customerRepository;

	private PhoneRepository phoneRepository;

	private CustomerSaveMapper customerSaveMapper;

	private CustomerMapper customerMapper;

	@Transactional
	@Override
	@CacheEvict(cacheNames = CustomerDTO.CACHE_NAME, allEntries = true)
	public CustomerDTO save(CustomerSaveDTO clienteSaveDTO) {
		try {
			log.info("Gravando o cliente: {}", clienteSaveDTO );

			log.info("Convertendo para entidade" );
			CustomerModel cliente = customerSaveMapper.toEntity(clienteSaveDTO);

			log.info("Gravando as informações no banco de dados" );
			CustomerModel clienteSave = customerRepository.save(cliente);

			log.info("{}" , clienteSave );
			log.info("Cliente gravado com sucesso" );
			return customerMapper.toDto(clienteSave);
		}catch( Exception e ) {
			throw new ApiException(ExceptionCodeEnum.ERRO_AO_TENTAR_GRAVAR_CLIENTE, e);
		}
	}

	@Transactional
	@Override
	@CachePut(cacheNames = CustomerDTO.CACHE_NAME)
	public CustomerDTO update(Long id, CustomerSaveDTO clienteSaveDTO) {
		log.info("Alterando as informações do cliente: {}", clienteSaveDTO );

		log.info("Removendo os telefones" );

		if (  BooleanUtils.isNotTrue( customerRepository.existsById(id) ) ) {
			throw new ApiException(ExceptionCodeEnum.ERRO_AO_TENTAR_ALTERAR_CLIENTE);
		}

		phoneRepository.deleteAllByCustomerId(id);

		Optional<CustomerModel> optional = customerRepository.findById(id);

		if(  optional.isEmpty () )  {
			throw new ApiException(ExceptionCodeEnum.CLIENTE_INEXISTENTE);
		}

		CustomerModel clienteModel =  customerSaveMapper.updateEntity(optional.get(), clienteSaveDTO ) ;

		log.info("Gravando as informações no banco de dados" );
		CustomerModel clienteSave = customerRepository.save(clienteModel);

		log.info("{}" , clienteSave );
		log.info("Cliente alterado com sucesso" );
		return customerMapper.toDto(clienteSave);
	}

	@Transactional
	@Override
	@CacheEvict(cacheNames = CustomerDTO.CACHE_NAME)
	public void remove(Long id) {
		log.info("Removendo o cliente com o Id: {}", id );

		Optional<CustomerModel> customer = customerRepository.findById(id);

		if( customer.isEmpty() ) {
			log.info("Não foi localizado cliente com Id : {}", id );
			throw new ApiException(ExceptionCodeEnum.ERRO_AO_TENTAR_REMOVER_CLIENTE);
		}

		customerRepository.delete(customer.get());
		customerRepository.flush();

		log.info("Removido com sucesso o cliente com o Id : {}", id );
		return;
	}

	@Override
	public PageDTO<CustomerDTO> findByCustomerFilter(CustomerFilter customerFilter, Integer page , Integer size ) {
		log.info("Buscando a lista de clientes com o filtro: {}", customerFilter );

		Pageable pageable = PageRequest.of(page, size, Sort.by("name"));

		Page<CustomerModel> pageCustomer = customerRepository.findByCustomerFilter(customerFilter, pageable);

		PageDTO<CustomerDTO> pageDTO = new PageDTO<>(
				customerMapper.toDto(pageCustomer.getContent()));
		pageDTO.isFirst(pageCustomer.isFirst());
		pageDTO.isLast(pageCustomer.isLast());
		pageDTO.setNumber(pageCustomer.getNumber());
		pageDTO.setNumberOfElements(pageCustomer.getNumberOfElements());
		pageDTO.setSize(pageCustomer.getSize());
		pageDTO.setTotalElements(pageCustomer.getTotalElements());
		pageDTO.setTotalPages(pageCustomer.getTotalPages());
		return pageDTO;
	}

	@Override
	@Cacheable(cacheNames = CustomerDTO.CACHE_NAME)
	public CustomerDTO findById(Long id) {
		log.info("Buscando o cliente por id {}", id);
		Optional<CustomerModel> cliente = customerRepository.findById(id);
		CustomerModel customerModel = cliente.orElseThrow( () ->  new ApiException( ExceptionCodeEnum.CLIENTE_INEXISTENTE ) );
		return customerMapper.toDto(customerModel);
	}

}
