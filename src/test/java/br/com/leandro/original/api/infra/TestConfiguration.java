package br.com.leandro.original.api.infra;

import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.core.util.ObjectMapperFactory;


@org.springframework.boot.test.context.TestConfiguration
public class TestConfiguration {

	@Bean
	public ObjectMapper objectMapper() {
		return ObjectMapperFactory.buildStrictGenericObjectMapper();
	}
}
