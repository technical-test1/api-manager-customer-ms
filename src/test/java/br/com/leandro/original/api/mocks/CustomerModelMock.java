package br.com.leandro.original.api.mocks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

import br.com.leandro.original.api.repository.model.CustomerModel;
import br.com.leandro.original.api.repository.model.AddressModel;
import br.com.leandro.original.api.repository.model.PhoneModel;

public class CustomerModelMock {

	public static CustomerModel customerModel() {
		CustomerModel customer = CustomerModel.builder().build();
		customer.setId(NumberUtils.LONG_ONE);
		customer.setCpf("11111111111");
		customer.setEmail("meuemail@meuemail.com.br");
		customer.setName("NOME CLIENTE TESTE");
		customer.setAddress(addressSave());
		customer.setPhones(phonesSave());
		return customer;
	}

	public static List<PhoneModel> phonesSave() {
		List<PhoneModel> phones = new ArrayList<>();
		phones.add( phone(1L,11L,989898888L));
		phones.add( phone(2L,11L,989898887L));
		phones.add( phone(3L,11L,989898886L));
		return phones;
	}

	public static PhoneModel phone(Long id, Long ddd, Long numero) {
		PhoneModel phone = PhoneModel.builder().build();
		phone.setId(id);
		phone.setDdd(ddd);
		phone.setNumber(numero);
		return phone;
	}

	public static AddressModel addressSave() {
		AddressModel address = AddressModel.builder().build();
		address.setId( NumberUtils.LONG_ONE);
		address.setDistrict("ITAQUERA");
		address.setZip("08233877");
		address.setCity("SAO PAULO");
		address.setComplement("APTO 10 1 ANDAR");
		address.setAddress("LADEIRA IDA E VOLDA");
		address.setNumber("555");
		address.setState("SP");
		return address;
	}
}
