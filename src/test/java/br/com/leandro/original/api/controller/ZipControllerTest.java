package br.com.leandro.original.api.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.leandro.original.api.infra.CacheConfig;
import br.com.leandro.original.api.infra.dto.ResponseDTO;
import br.com.leandro.original.api.infra.handler.exception.ApiException;
import br.com.leandro.original.api.service.dto.viacep.AddressViaCep;
import br.com.leandro.original.api.service.impl.SearchViaCepService;
import br.com.leandro.original.api.service.impl.ViaCepServiceImpl;
import feign.FeignException;

@ActiveProfiles(profiles = { "test" })
@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest( classes = { ZipController.class, br.com.leandro.original.api.infra.TestConfiguration.class, CacheConfig.class, ViaCepServiceImpl.class })
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class ZipControllerTest {

	private final String responseJson = "{\n" +
			"  \"cep\": \"08290-370\",\n" +
			"  \"logradouro\": \"Rua Serra de São Domingos\",\n" +
			"  \"complemento\": \"\",\n" +
			"  \"bairro\": \"Vila Carmosina\",\n" +
			"  \"localidade\": \"São Paulo\",\n" +
			"  \"uf\": \"SP\",\n" +
			"  \"ibge\": \"3550308\",\n" +
			"  \"gia\": \"1004\",\n" +
			"  \"ddd\": \"11\",\n" +
			"  \"siafi\": \"7107\"\n" +
			"}";

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ViaCepServiceImpl viaCepService;

	@MockBean
	private SearchViaCepService searchViaCepService;

	private ZipController zipController;

	@Before
	public void setup() {
		zipController = new ZipController(viaCepService);
	}

	@Test
	public void test_search_zip_success() throws Exception{

		AddressViaCep addressViaCep = objectMapper.readValue(responseJson , AddressViaCep.class );

		BDDMockito
		.when( searchViaCepService.findByZip( ArgumentMatchers.anyString()))
		.thenReturn(addressViaCep);


		ResponseEntity<ResponseDTO<AddressViaCep>> response = zipController.findById("08290370");

		ResponseDTO<AddressViaCep> resultResponse = response.getBody();

		assertNotNull( resultResponse.getData() );
		assertEquals( "08290-370", resultResponse.getData().getCep() );
	}

	@Test(expected = ApiException.class )
	public void test_search_zip_exception() throws Exception{

		BDDMockito
		.when( searchViaCepService.findByZip( ArgumentMatchers.anyString()))
		.thenThrow(FeignException.class);

		zipController.findById("08290370");

	}

}
