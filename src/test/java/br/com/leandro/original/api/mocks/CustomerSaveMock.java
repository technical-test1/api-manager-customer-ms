package br.com.leandro.original.api.mocks;

import java.util.ArrayList;
import java.util.List;

import br.com.leandro.original.api.service.dto.CustomerSaveDTO;
import br.com.leandro.original.api.service.dto.AddressSaveDTO;
import br.com.leandro.original.api.service.dto.PhoneSaveDTO;

public class CustomerSaveMock {

	public static CustomerSaveDTO customerSaveDTO() {
		CustomerSaveDTO customer = CustomerSaveDTO.builder().build();
		customer.setName("NOME CLIENTE TESTE");
		customer.setEmail("email@email.com.br");
		customer.setAddress(addressSave());
		customer.setCpf("11111111111");
		customer.setPhones(phonesSave());
		return customer;
	}

	public static List<PhoneSaveDTO> phonesSave() {
		List<PhoneSaveDTO> phones = new ArrayList<>();
		phones.add( phoneSave(11L,989898888L));
		phones.add( phoneSave(11L,989898887L));
		phones.add( phoneSave(11L,989898886L));
		return phones;
	}

	public static PhoneSaveDTO phoneSave(Long ddd, Long numero) {
		PhoneSaveDTO phones = PhoneSaveDTO.builder().build();
		phones.setDdd(ddd);
		phones.setNumber(numero);
		return phones;
	}

	public static AddressSaveDTO addressSave() {
		AddressSaveDTO address = AddressSaveDTO.builder().build();
		address.setDistrict("ITAQUERA");
		address.setZip("08233877");
		address.setCity("SAO PAULO");
		address.setComplement("APTO 10 1 ANDAR");
		address.setAddress("LADEIRA IDA E VOLDA");
		address.setNumber("555");
		address.setState("SP");
		return address;
	}
}
