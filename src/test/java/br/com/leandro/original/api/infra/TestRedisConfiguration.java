package br.com.leandro.original.api.infra;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;

import redis.embedded.RedisServer;


@org.springframework.boot.test.context.TestConfiguration
public class TestRedisConfiguration {

	private RedisServer redisServer;


    public TestRedisConfiguration( @Value("${spring.redis.port}") int redisPort) {
    	this.redisServer =
    			RedisServer.builder()
    			.setting("maxheap 512Mb")
    			.port(redisPort)
    			.build();
    }

    @PostConstruct
    public void postConstruct() {
        redisServer.start();
    }

    @PreDestroy
    public void preDestroy() {
        redisServer.stop();
    }
}
