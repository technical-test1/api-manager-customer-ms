package br.com.leandro.original.api.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.math.NumberUtils;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.leandro.original.api.SpringWebApp;
import br.com.leandro.original.api.infra.TestConfiguration;
import br.com.leandro.original.api.infra.TestRedisConfiguration;
import br.com.leandro.original.api.infra.dto.ResponseDTO;
import br.com.leandro.original.api.infra.dto.ResponseErrorDTO;
import br.com.leandro.original.api.mocks.CustomerModelMock;
import br.com.leandro.original.api.mocks.CustomerSaveMock;
import br.com.leandro.original.api.repository.CustomerRepository;
import br.com.leandro.original.api.repository.PhoneRepository;
import br.com.leandro.original.api.repository.model.CustomerModel;
import br.com.leandro.original.api.service.dto.CustomerDTO;
import br.com.leandro.original.api.service.dto.CustomerSaveDTO;
import br.com.leandro.original.api.service.dto.PhoneSaveDTO;
import br.com.leandro.original.api.service.filter.CustomerFilter;


@AutoConfigureMockMvc
@ActiveProfiles(profiles = { "test" })
@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(classes = { SpringWebApp.class, TestConfiguration.class, TestRedisConfiguration.class  })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
class CustomerControllerTests {

	private final static String URL = "/api/customer/v1/customers";

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private CustomerRepository customerRepository;

	@MockBean
	private PhoneRepository phoneRepository;

	@Test
	void test_save_customer_with_success() throws Exception {

		BDDMockito
			.when( customerRepository.save(ArgumentMatchers.any(CustomerModel.class)))
			.thenReturn( CustomerModelMock.customerModel());

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
					.post(URL)
					.contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(CustomerSaveMock.customerSaveDTO())))
				.andExpect(MockMvcResultMatchers
							.status()
							.isCreated())
			.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseDTO<CustomerDTO> response = objectMapper.readValue(contentAsString,
				new TypeReference<ResponseDTO<CustomerDTO>>() {
				});
		assertNotNull( response.getData().getId() );
	}


	@Test
	void test_try_save_customer_returned_exception() throws Exception {

		BDDMockito
			.when( customerRepository.save(ArgumentMatchers.any(CustomerModel.class)))
			.thenThrow( RuntimeException.class );

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
					.post(URL)
					.contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(CustomerSaveMock.customerSaveDTO())))
				.andExpect(MockMvcResultMatchers
							.status()
							.isBadRequest())
			.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseErrorDTO response = objectMapper.readValue(contentAsString,ResponseErrorDTO.class);
		assertNotNull( response.getError() );
	}

	@Test
	void test_try_save_customer_with_information_invalid_name_and_phones_null() throws Exception {

		CustomerSaveDTO clienteSaveDTO = CustomerSaveMock.customerSaveDTO();
		clienteSaveDTO.setName( null );
		clienteSaveDTO.setEmail(null);
		clienteSaveDTO.setPhones(null);
		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
					.post(URL)
					.contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(clienteSaveDTO)))
				.andExpect(MockMvcResultMatchers
							.status()
							.isUnprocessableEntity())
			.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseErrorDTO response = objectMapper.readValue(contentAsString,ResponseErrorDTO.class);

		assertNotNull( response.getError() );
	}


	@Test
	void test_try_save_customer_with_phones_invalid() throws Exception {

		CustomerSaveDTO clienteSaveDTO = CustomerSaveMock.customerSaveDTO();

		List<PhoneSaveDTO> phones = clienteSaveDTO.getPhones();//
		phones.add(CustomerSaveMock.phoneSave(null, null));
		clienteSaveDTO.setPhones(phones);

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
					.post(URL)
					.contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(clienteSaveDTO)))
				.andExpect(MockMvcResultMatchers
							.status()
							.isUnprocessableEntity())
			.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseErrorDTO response = objectMapper.readValue(contentAsString,ResponseErrorDTO.class);

		assertNotNull( response.getError() );
	}

	@Test
	void test_update_customer_wiht_success() throws Exception {

		BDDMockito
			.when( customerRepository.existsById(ArgumentMatchers.anyLong()) )
			.thenReturn( Boolean.TRUE );

		BDDMockito
			.when( customerRepository.findById(ArgumentMatchers.anyLong()))
			.thenReturn( Optional.of( CustomerModelMock.customerModel()));

		BDDMockito
			.doAnswer( new Answer<Void>() {
				@Override
				public Void answer(InvocationOnMock invocation) throws Throwable {
					return null;
				}
			})
			.when(phoneRepository)
			.deleteAllByCustomerId( ArgumentMatchers.anyLong());

		BDDMockito
			.when( customerRepository.findById(ArgumentMatchers.anyLong()))
			.thenReturn( Optional.of( CustomerModelMock.customerModel()));

		BDDMockito
			.when( customerRepository.save(ArgumentMatchers.any(CustomerModel.class)))
			.thenReturn( CustomerModelMock.customerModel());

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
					.put(URL.concat("/1") )
					.contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(CustomerSaveMock.customerSaveDTO())))
				.andExpect(MockMvcResultMatchers
							.status()
							.isOk())
			.andReturn();

		String contentAsString = result.getResponse().getContentAsString();


		ResponseDTO<CustomerDTO> response = objectMapper.readValue(contentAsString,
				new TypeReference<ResponseDTO<CustomerDTO>>() {
				});
		assertNotNull( response.getData().getId() );
	}

	@Test
	void test_try_update_customer_not_found() throws Exception {

		BDDMockito
			.when( customerRepository.existsById(ArgumentMatchers.anyLong()) )
			.thenReturn( Boolean.FALSE );

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				    .put(URL.concat("/2"))
					.contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(CustomerSaveMock.customerSaveDTO())))
				.andExpect(MockMvcResultMatchers
							.status()
							.isBadRequest())
			.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseErrorDTO response = objectMapper.readValue(contentAsString,ResponseErrorDTO.class);

		assertNotNull( response.getError() );
	}

	@Test
	void test_try_update_customer_invalid_information_name_and_phones() throws Exception {

		CustomerSaveDTO clienteSaveDTO = CustomerSaveMock.customerSaveDTO();
		clienteSaveDTO.setName( null );
		clienteSaveDTO.setPhones(null);
		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				    .put(URL.concat("/1"))
					.contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(clienteSaveDTO)))
				.andExpect(MockMvcResultMatchers
							.status()
							.isUnprocessableEntity())
			.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseErrorDTO response = objectMapper.readValue(contentAsString,ResponseErrorDTO.class);

		assertNotNull( response.getError() );
	}

	@Test
	void test_try_update_customer_with_phone_invalid() throws Exception {

		CustomerSaveDTO customerSaveDTO = CustomerSaveMock.customerSaveDTO();
		customerSaveDTO.getPhones().add(CustomerSaveMock.phoneSave(null, null));
		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				    .put(URL.concat("/1"))
					.contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(customerSaveDTO)))
				.andExpect(MockMvcResultMatchers
							.status()
							.isUnprocessableEntity())
			.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseErrorDTO response = objectMapper.readValue(contentAsString,ResponseErrorDTO.class);

		assertNotNull( response.getError() );
	}


	@Test
	void test_try_remove_a_customer_found() throws Exception {

		BDDMockito
		.when( customerRepository.findById(ArgumentMatchers.anyLong()) )
		.thenReturn( Optional.of( CustomerModel.builder().id(NumberUtils.LONG_ONE).build() ) );

		BDDMockito
		.doAnswer( new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				return null;
			}})
		.when(customerRepository).deleteById(ArgumentMatchers.anyLong());

		BDDMockito
		.doAnswer( new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				return null;
			}})
		.when(customerRepository).flush();


		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.delete( URL.concat("/1"))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers
						.status()
						.isOk())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseDTO<String> response = objectMapper.readValue(contentAsString,
				new TypeReference<ResponseDTO<String>>() {
		});
		assertNotNull( response.getData() );
	}

	@Test
	void test_try_remove_a_customer_not_found() throws Exception {

		BDDMockito
		.when( customerRepository.findById(ArgumentMatchers.anyLong()) )
		.thenReturn( Optional.empty() );

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.delete(URL.concat("/222"))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers
						.status()
						.isBadRequest())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		ResponseErrorDTO response = objectMapper.readValue(contentAsString,ResponseErrorDTO.class);

		assertNotNull( response.getError() );
	}

	@Test
	void test_find_with_response_empty() throws Exception {

		Page<CustomerModel> pageCustomer
			= new PageImpl<>(new ArrayList<CustomerModel>() ,PageRequest.of(0, 15, Sort.by("name")) , 0L);

		BDDMockito.when( customerRepository.findByCustomerFilter( ArgumentMatchers.any(CustomerFilter.class), ArgumentMatchers.any(Pageable.class)))
		.thenReturn(pageCustomer);

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.get(URL)
				.param("name", "NAME TESTE")
				.param("email", "email@test.com")
				.param("page", "0")
				.param("size", "15")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers
						.status()
						.isNoContent())
				.andReturn();

		assertEquals( HttpStatus.NO_CONTENT.value(), result.getResponse().getStatus() );
	}

	@Test
	void test_find_with_response_not_empty() throws Exception {

		ArrayList<CustomerModel> content = new ArrayList<CustomerModel>();
		content.add( CustomerModelMock.customerModel());
		Page<CustomerModel> pageCustomer
		= new PageImpl<>(content ,PageRequest.of(0, 15, Sort.by("name")) , 0L);

		BDDMockito.when( customerRepository.findByCustomerFilter( ArgumentMatchers.any(CustomerFilter.class), ArgumentMatchers.any(Pageable.class)))
		.thenReturn(pageCustomer);

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.get(URL)
				.param("name", "NAME TESTE")
				.param("email", "email@test.com")
				.param("page", "0")
				.param("size", "15")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers
						.status()
						.isOk())
				.andReturn();

		assertEquals( HttpStatus.OK.value(), result.getResponse().getStatus() );
	}

	@Test
	void test_find_by_id_with_sucess() throws Exception {

		BDDMockito.when( customerRepository.findById(ArgumentMatchers.anyLong()) ).thenReturn( Optional.of( CustomerModelMock.customerModel()));


		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.get(URL.concat("/1"))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers
						.status()
						.isOk())
				.andReturn();

		assertEquals( HttpStatus.OK.value(), result.getResponse().getStatus() );
	}

	@Test
	void test_find_by_id_not_found() throws Exception {

		BDDMockito.when( customerRepository.findById(ArgumentMatchers.anyLong()) ).thenReturn( Optional.empty());


		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.get(URL.concat("/2"))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers
						.status()
						.isBadRequest())
				.andReturn();

		assertEquals( HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus() );
	}

}
